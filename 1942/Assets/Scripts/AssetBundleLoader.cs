﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GGStudios.Controllers;
using System.IO;

public class AssetBundleLoader : MonoBehaviour
{
    [SerializeField] private List<AssetBundleObject> assetBundlesList = new List<AssetBundleObject>();
    [SerializeField] private Dictionary<string, GameObject> nameObjectDictionary = new Dictionary<string, GameObject>();
    private bool assetsLoaded = false;

    public void Initialize()
    {
        StartCoroutine(LoadAssetBundle());
    }
    public Dictionary<string, GameObject> NameObjectDictionary
    {
        get
        {
            return nameObjectDictionary;
        }

        set
        {
            nameObjectDictionary = value;
        }
    }

    AssetBundleCreateRequest bundleLoadRequest;
    public IEnumerator LoadAssetBundle()
    {
        for (int i = 0; i < assetBundlesList.Count; i++)
        {
            GameController.GetInstance().SetLoadingItemName(assetBundlesList[i].AssetBundleFileName + "Loading");
            bundleLoadRequest = AssetBundle.LoadFromFileAsync(Path.Combine(Application.streamingAssetsPath, assetBundlesList[i].AssetBundleFileName));
            yield return bundleLoadRequest;

            var myLoadedAssetBundle = bundleLoadRequest.assetBundle;
            if (myLoadedAssetBundle == null)
            {
                Debug.Log("Failed to load AssetBundle!");
                yield break;
            }
            string[] allAssetNames = myLoadedAssetBundle.GetAllAssetNames();
            for (int z = 0; z < allAssetNames.Length; z++)
            {
                Debug.Log("Asset names: " + allAssetNames[z]);
                var assetLoadRequest = myLoadedAssetBundle.LoadAssetAsync<GameObject>(allAssetNames[z]);
                yield return assetLoadRequest;
                GameObject prefab = assetLoadRequest.asset as GameObject;
                nameObjectDictionary.Add(assetBundlesList[i].AssetBundleGameObjectName + z, prefab);
            }
            myLoadedAssetBundle.Unload(false);
        }
        assetsLoaded = true;
        GameController.GetInstance().FinishLoadingAssets();
    }

    private void Update()
    {
        if(bundleLoadRequest != null)
        {
            GameController.GetInstance().SetLoadingProgress(bundleLoadRequest.progress);
        }
    }

}

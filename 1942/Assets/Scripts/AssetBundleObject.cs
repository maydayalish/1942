﻿[System.Serializable]
public class AssetBundleObject
{
    public string AssetBundleFileName;
    public string AssetBundleGameObjectName;

    public AssetBundleObject(string assetBundleFileName, string assetBundleGameObjectName)
    {
        AssetBundleFileName = assetBundleFileName;
        AssetBundleGameObjectName = assetBundleGameObjectName;
    }
}

﻿using UnityEngine;

public class AssetData 
{
    public GameObject EnemyPlanePrefab;
    public GameObject PlayerBulletPrefab;
    public GameObject EnemyBulletPrefab;
}

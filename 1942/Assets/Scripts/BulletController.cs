﻿using UnityEngine;

namespace GGStudios.Controllers
{
    public class BulletController : PoolObject
    {
        public void Initialize(Vector3 startPos, Vector3 targetPos)
        {
            (movementController as IAIMovement).Initialize(startPos, targetPos);
        }
        private void OnCollisionEnter(Collision collision)
        {
            Debug.Log("Bullet hit");
            PlaneController plane = collision.gameObject.GetComponent<PlaneController>();
            plane.Hit();
            GoBackToPool();
        }
    }
}


﻿using System.Collections;
using UnityEngine;

namespace GGStudios.Controllers
{
    public class EnemyPlaneController : PlaneController
    {
        [SerializeField] private int point;
        [SerializeField] private float fireRate;

        public void Initialize(Vector3 startPos, Vector3 targetPos, int point, int health, FireType fireType, float fireRate)
        {
            this.point = point;
            this.Health = health;
            this.fireRate = fireRate;
            Debug.Log("Fire Type: " + fireType);
            switch (fireType)
            {
                case FireType.Random: Invoke("InvokedStartRandomFire", 2.5f); break;
                case FireType.OnTarget: Invoke("InvokedStartOnTargetFire", 2.5f); break;
            }
            (movementController as IAIMovement).Initialize(startPos, targetPos);
        }
        
        public void InvokedStartRandomFire()
        {
            StartCoroutine(RandomFire());
        }

        public void InvokedStartOnTargetFire()
        {
            StartCoroutine(OnTargetFire());
        }

        public IEnumerator RandomFire()
        {

            Vector3 fireDirection = new Vector3(Random.Range(-1f, 1f), 0, Random.Range(-1f, 1)).normalized;
            GameController.GetInstance().ObjectPool.SpawnEnemyBullet(transform.position, transform.position + fireDirection);
            yield return new WaitForSeconds(fireRate);
            StartCoroutine(RandomFire());
        }

        public IEnumerator OnTargetFire()
        {
            GameController.GetInstance().ObjectPool.SpawnEnemyBullet(transform.position, GameController.GetInstance().Player.transform.position);
            yield return new WaitForSeconds(fireRate);
            StartCoroutine(OnTargetFire());
        }

        public override void Hit()
        {
            Debug.Log("Hit");
            Health--;
            if (Health == 0)
            {
                GameController.GetInstance().AddScore(point);
                GoBackToPool();
            }
        }

        private void OnCollisionEnter(Collision collision)
        {
            if(collision.gameObject.tag.Equals("PlayerPlane"))
            {
                GameController.GetInstance().AddScore(point);
                GoBackToPool();
                collision.gameObject.GetComponent<PlayerController>().Hit();
            }
        }

        public int Point
        {
            get
            {
                return point;
            }

            set
            {
                point = value;
            }
        }
    }
}


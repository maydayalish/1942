﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GGStudios.Controllers
{
    public class EnvironmentController : MonoBehaviour
    {
        [SerializeField] private GameObject waterPrefab;
        private List<EnvironmentObject> environmenObjectsPool = new List<EnvironmentObject>();

        public void Initialize(GameObject wPrefab = null)
        {
            if (wPrefab != null)
            {
                waterPrefab = wPrefab;
            }
            if (waterPrefab == null)
            {
                //WarningUI gives an error
                return;
            }
            CreateEnvironmentPool();
        }
        

        public void CreateEnvironmentPool()
        {
            for (int i = 0; i < 2; i++)
            {
                EnvironmentObject environmentObject = Instantiate(waterPrefab, new Vector3(i * 200, 0, 0), Quaternion.identity).GetComponent<EnvironmentObject>();
                environmenObjectsPool.Add(environmentObject);
                environmentObject.Initialize();
            }
        }
    }
}


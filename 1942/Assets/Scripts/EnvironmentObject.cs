﻿using System.Collections.Generic;
using UnityEngine;
using GGStudios.Controllers;

public class EnvironmentObject : MonoBehaviour
{
    private List<GameObject> passiveObjectsPool = new List<GameObject>();
    private List<GameObject> activeObjectsPool = new List<GameObject>();
    private bool moving = true;

    private void Update()
    {
        if(moving)
        {
            transform.Translate(new Vector3(-5f * Time.deltaTime, 0, 0));
            if (transform.position.x < -200)
            {
                transform.position = new Vector3(transform.position.x + 400, 0, 0);
                RenewObjects();
            }
        }
    }

    public void Initialize()
    {
        for (int i = 0; i < 15; i++)
        {
            GameObject environmentObject = GameController.GetInstance().GetAssetByName("Environment" + i);
            GameObject newObject = Instantiate(environmentObject, new Vector3((int)(i/3) * 40 - 80, 21.5f, i%3 * 25 - 25), Quaternion.identity);
            passiveObjectsPool.Add(newObject);
            newObject.transform.SetParent(this.transform, false);
            newObject.SetActive(false);
        }
        RenewObjects();
        GameController.GetInstance().OnGameFinished += FinishGame;
    }

    private void FinishGame()
    {
        moving = false;
    }

    public void RenewObjects()
    {
        for (int i = 0; i < activeObjectsPool.Count; i++)
        {
            activeObjectsPool[i].SetActive(false);
            passiveObjectsPool.Add(activeObjectsPool[i]);
            activeObjectsPool.Remove(activeObjectsPool[i]);
            i--;
        }
        for (int i = 0; i < 6; i++)
        {
            GameObject objectToActivate = passiveObjectsPool[Random.Range(0, passiveObjectsPool.Count)];
            objectToActivate.SetActive(true);
            activeObjectsPool.Add(objectToActivate);
            passiveObjectsPool.Remove(objectToActivate);
        }
    }
}

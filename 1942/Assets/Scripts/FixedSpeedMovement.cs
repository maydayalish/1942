﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GGStudios.Controllers
{
    public class FixedSpeedMovement : UpdateMovement, IAIMovement
    {
        private Vector3 movementDirection;

        public Vector3 MovementDirection
        {
            get
            {
                return movementDirection;
            }

            set
            {
                movementDirection = value;
            }
        }

        public void Initialize(Vector3 startPosition, Vector3 targetPosition)
        {
            movementDirection = (startPosition - targetPosition).normalized;
            gameObject.SetActive(true);
        }

        public override void Move()
        {
            transform.Translate(movementDirection * Time.deltaTime * Speed);
        }
    }
}


﻿using UnityEngine;
using GGStudios.Pool;
using System.Collections;
using UnityEngine.SceneManagement;

namespace GGStudios.Controllers
{
    public class GameController : MonoBehaviour
    {
        private static GameController singleton = null;

        public delegate void SimpleCallback();
        public event SimpleCallback OnGameFinished;

        private int score;
        private int currentLevel;

        [SerializeField] private EnvironmentController environmentController;
        [SerializeField] private SpawnController spawnController;
        [SerializeField] private GameRules gameRules;
        [SerializeField] private PlayerController player;
        [SerializeField] private UIController uiController;
        [SerializeField] private AssetBundleLoader assetBundleLoader;


        public InputManager InputManager;
        public ObjectPool ObjectPool;

        public static GameController GetInstance()
        {
            return singleton;
        }

        private void Awake()
        {
            singleton = this;
            assetBundleLoader.Initialize();
        }

        public void Initialize()
        {
            environmentController.Initialize();
            ObjectPool.Initialize();
            spawnController.Initialize();
            uiController.Initialize(gameRules.PlayerHealth);
            player.Health = gameRules.PlayerHealth;
        }
        
        public void AddScore(int scoreToAdd)
        {
            score += scoreToAdd;
            uiController.SetScore(score);
            if(currentLevel < gameRules.PointLevel.Length)
            {
                if (score >= gameRules.PointLevel[currentLevel])
                {
                    currentLevel++;
                    uiController.SetLevel(currentLevel + 1);
                }
            }
        }

        public void SetHealth(int health)
        {
            if(health == 0)
            {
                FinishGame();
            }
            uiController.SetHealth(health);
        }

        public int CurrentLevel
        {
            get
            {
                return currentLevel;
            }

            set
            {
                currentLevel = value;
            }
        }

        public PlayerController Player
        {
            get
            {
                return player;
            }

            set
            {
                player = value;
            }
        }

        public float GetSpawnInterval()
        {
            if(gameRules.SpawnPlaneIntervalByLevel.Length > currentLevel)
            {
                return gameRules.SpawnPlaneIntervalByLevel[currentLevel];
            }
            return gameRules.SpawnPlaneIntervalByLevel[gameRules.SpawnPlaneIntervalByLevel.Length - 1];
        }

        public int GetLevelPlanePoint()
        {
            if(gameRules.PointPerObjectLevel.Length > currentLevel)
            {
                return gameRules.PointPerObjectLevel[currentLevel];
            }
            return gameRules.PointPerObjectLevel[gameRules.PointPerObjectLevel.Length - 1];
        }

        public float GetFireRate()
        {
            return gameRules.PlayerFireRate;
        }

        public void FinishGame()
        {
            player.gameObject.SetActive(false);
            ObjectPool.DestroyAllObjects();
            uiController.GameOverPanel.Initialize(score);
            if(OnGameFinished != null)
            {
                OnGameFinished();
            }
            StartCoroutine(FinishCoroutine());
        }

        public IEnumerator FinishCoroutine()
        {
            yield return new WaitForSeconds(4f);
            SceneManager.LoadScene("Game");
        }
        
        public void SetLoadingItemName(string itemName)
        {
            uiController.StartPanel.LoadingAssetName.text = itemName;
        }

        public void SetLoadingProgress(float progress)
        {
            uiController.StartPanel.SetProgressBar(progress);
        }

        public GameObject GetAssetByName(string assetName)
        {
            return assetBundleLoader.NameObjectDictionary[assetName];
        }

        public void FinishLoadingAssets()
        {
            uiController.StartPanel.OnAssetsLoaded();
        }
    }
}


﻿using UnityEngine;
using UnityEngine.UI;

public class GameOverPanelUI : MonoBehaviour
{
    public Text ScoreText;
    public Text HighScoreText;

    public void Initialize(int score)
    {
        if(PlayerPrefs.HasKey("HighScore"))
        {
            if(score > PlayerPrefs.GetInt("HighScore"))
            {
                PlayerPrefs.SetInt("HighScore", score);
            }
        }
        else
        {
            PlayerPrefs.SetInt("HighScore", score);
        }
        ScoreText.text = "Your Score: " + score;
        HighScoreText.text = "Highest Score " + PlayerPrefs.GetInt("HighScore");
        gameObject.SetActive(true);
    }
}

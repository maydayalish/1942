﻿using UnityEngine;

[System.Serializable]
public class GameRules
{
    [SerializeField] private int[] pointPerObjectLevel;
    [SerializeField] private float[] spawnPlaneIntervalByLevel;
    [SerializeField] private int[] pointLevel;
    [SerializeField] private float playerFireRate;
    [SerializeField] private int playerHealth;

    public int[] PointPerObjectLevel
    {
        get
        {
            return pointPerObjectLevel;
        }

        set
        {
            pointPerObjectLevel = value;
        }
    }

    public float[] SpawnPlaneIntervalByLevel
    {
        get
        {
            return spawnPlaneIntervalByLevel;
        }

        set
        {
            spawnPlaneIntervalByLevel = value;
        }
    }

    public int[] PointLevel
    {
        get
        {
            return pointLevel;
        }

        set
        {
            pointLevel = value;
        }
    }

    public float PlayerFireRate
    {
        get
        {
            return playerFireRate;
        }

        set
        {
            playerFireRate = value;
        }
    }

    public int PlayerHealth
    {
        get
        {
            return playerHealth;
        }

        set
        {
            playerHealth = value;
        }
    }
}

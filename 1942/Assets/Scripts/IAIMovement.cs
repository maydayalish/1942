﻿using UnityEngine;

interface IAIMovement
{
    Vector3 MovementDirection { get; set; }
    void Initialize(Vector3 startPosition, Vector3 targetPosition);
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour {

    public KeyCode LeftButton;
    public KeyCode RightButton;
    public KeyCode ForwardButton;
    public KeyCode BackwardButton;
    public KeyCode FireButton;
}

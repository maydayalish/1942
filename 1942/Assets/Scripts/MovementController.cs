﻿using UnityEngine;

namespace GGStudios.Controllers
{
    [System.Serializable]
    public abstract class MovementController : MonoBehaviour
    {
        [SerializeField] private float speed;
        [SerializeField] private Boundary boundary;
        public delegate void SimpleCallback();
        public event SimpleCallback OnGoBackPool;

        public abstract void Move();

        public float Speed
        {
            get
            {
                return speed;
            }

            set
            {
                speed = value;
            }
        }

        public Boundary Boundary
        {
            get
            {
                return boundary;
            }

            set
            {
                boundary = value;
            }
        }
        
        public void GoBackToPool()
        {
            if(OnGoBackPool != null)
            {
                OnGoBackPool();
            }
        }

        public void CheckOutOfBound()
        {
            if (transform.position.x < Boundary.XMin || transform.position.x > Boundary.XMax || transform.position.z < Boundary.YMin || transform.position.z > Boundary.YMax)
            {
                GoBackToPool();
            }
        }
    }
}


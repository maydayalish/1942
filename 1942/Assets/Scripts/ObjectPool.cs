﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GGStudios.Controllers;

namespace GGStudios.Pool
{
    public class ObjectPool : MonoBehaviour
    {
        private GameObject enemyPlanePrefab;
        private GameObject playerBulletPrefab;
        private GameObject enemyBulletPrefab;

        private List<EnemyPlaneController> enemyPlanePassivePool = new List<EnemyPlaneController>();
        private List<EnemyPlaneController> enemyPlaneActivePool = new List<EnemyPlaneController>();
        private List<BulletController> playerBulletPassivePool = new List<BulletController>();
        private List<BulletController> playerBulletActivePool = new List<BulletController>();
        private List<BulletController> enemyBulletPassivePool = new List<BulletController>();
        private List<BulletController> enemyBulletActivePool = new List<BulletController>();

        public void Initialize()
        {
            CreateEnemyPlanePool();
            CreatePlayerBulletPool();
            CreateEnemyBulletPool();
        }

        public void CreateEnemyPlanePool()
        {
            enemyPlanePrefab = GameController.GetInstance().GetAssetByName("AirplaneEnemy0");
            for (int i = 0; i < 10; i++)
            {
                EnemyPlaneController enemyPlane = Instantiate(enemyPlanePrefab).GetComponent<EnemyPlaneController>();
                enemyPlane.Initialize();
                enemyPlane.gameObject.SetActive(false);
                enemyPlanePassivePool.Add(enemyPlane);
            }
        }

        public void CreatePlayerBulletPool()
        {
            playerBulletPrefab = GameController.GetInstance().GetAssetByName("PlayerBullet0");
            for (int i = 0; i < 10; i++)
            {
                BulletController playerBullet = Instantiate(playerBulletPrefab).GetComponent<BulletController>();
                playerBullet.gameObject.SetActive(false);
                playerBullet.Initialize();
                playerBulletPassivePool.Add(playerBullet);
            }
        }

        public void CreateEnemyBulletPool()
        {
            enemyBulletPrefab = GameController.GetInstance().GetAssetByName("EnemyBullet0");
            for (int i = 0; i < 20; i++)
            {
                BulletController enemyBullet = Instantiate(enemyBulletPrefab).GetComponent<BulletController>();
                enemyBullet.gameObject.SetActive(false);
                enemyBullet.Initialize();
                enemyBulletPassivePool.Add(enemyBullet);
            }
        }

        public void GoBackToPool(PoolObject poolObject)
        {
            poolObject.gameObject.SetActive(false);
            switch(poolObject.tag)
            {
                case "EnemyPlane":
                    EnemyPlaneController enemyPlane = poolObject as EnemyPlaneController;
                    enemyPlaneActivePool.Remove(enemyPlane);
                    enemyPlanePassivePool.Add(enemyPlane);
                    break;
                case "PlayerBullet":
                    BulletController playerBullet = poolObject as BulletController;
                    playerBulletActivePool.Remove(playerBullet);
                    playerBulletPassivePool.Add(playerBullet);
                    break;
                case "EnemyBullet":
                    BulletController enemyBullet = poolObject as BulletController;
                    enemyBulletActivePool.Remove(enemyBullet);
                    enemyBulletPassivePool.Add(enemyBullet);
                    break;
            }
        }

        public void SpawnEnemyPlane(Vector3 spawnPosition, Vector3 targetPosition,int planePoint, int planeHealth, FireType planeFireType)
        {
            if(enemyPlanePassivePool.Count > 0)
            {
                EnemyPlaneController enemyPlane = enemyPlanePassivePool[0];
                enemyPlanePassivePool.Remove(enemyPlane);
                enemyPlaneActivePool.Add(enemyPlane);
                enemyPlane.transform.position = spawnPosition;
                enemyPlane.Initialize(spawnPosition, targetPosition, planePoint, planeHealth, planeFireType, 5 /*It can be customized on level*/);
            }
        }

        public void SpawnEnemyBullet(Vector3 spawnPosition, Vector3 targetPosition)
        {
            if (enemyBulletPassivePool.Count > 0)
            {
                BulletController enemyBullet = enemyBulletPassivePool[0];
                enemyBulletPassivePool.Remove(enemyBullet);
                enemyBulletActivePool.Add(enemyBullet);
                enemyBullet.transform.position = spawnPosition;
                enemyBullet.Initialize(spawnPosition, targetPosition);
            }
        }

        public void SpawnPlayerBullet(Vector3 spawnPosition, Vector3 targetPosition)
        {
            if (playerBulletPassivePool.Count > 0)
            {
                BulletController playerBullet = playerBulletPassivePool[0];
                playerBulletPassivePool.Remove(playerBullet);
                playerBulletActivePool.Add(playerBullet);
                playerBullet.transform.position = spawnPosition;
                playerBullet.Initialize(spawnPosition, targetPosition);
            }
        }

        public void DestroyAllObjects()
        {
            for (int i = 0; i < enemyBulletActivePool.Count; i++)
            {
                enemyBulletActivePool[i].gameObject.SetActive(false);
            }
            for (int i = 0; i < playerBulletActivePool.Count; i++)
            {
                playerBulletActivePool[i].gameObject.SetActive(false);
            }
            for (int i = 0; i < enemyPlaneActivePool.Count; i++)
            {
                enemyPlaneActivePool[i].gameObject.SetActive(false);
            }
        }
    }
}


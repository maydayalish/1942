﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PlaneController : PoolObject
{
    [SerializeField] protected int health;

    public int Health
    {
        get
        {
            return health;
        }
        set
        {
            health = value;
        }
    }

    public abstract void Hit();
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GGStudios.Controllers
{
    public class PlayerController : PlaneController
    {
        private float fireTimer;
        private bool canFire;

        public override void Hit()
        {
            health--;
            GameController.GetInstance().SetHealth(health);
        }
        
        private void Update()
        {
            if(Input.GetKey(GameController.GetInstance().InputManager.FireButton))
            {
                if(canFire)
                {
                    Fire();
                    fireTimer = GameController.GetInstance().GetFireRate();
                    canFire = false;
                }
            }
            if(!canFire)
            {
                fireTimer -= Time.deltaTime;
                if(fireTimer <= 0)
                {
                    canFire = true;
                }
            }
        }

        private void OnCollisionEnter(Collision collision)
        {

        }

        public void Fire()
        {
            GameController.GetInstance().ObjectPool.SpawnPlayerBullet(transform.position, new Vector3(1,0,0) + transform.position);
        }
    }
}


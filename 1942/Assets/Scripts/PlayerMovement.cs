﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GGStudios.Controllers;

namespace GGStudios.Controllers
{
    public class PlayerMovement : UpdateMovement
    {
        public override void Move()
        {
            Vector3 nextPos = transform.position;
            Vector3 movementDif = new Vector3(0,0,0);
            if (Input.GetKey(GameController.GetInstance().InputManager.LeftButton))
            {
                movementDif.z += Time.deltaTime;
            }

            if (Input.GetKey(GameController.GetInstance().InputManager.RightButton))
            {
                movementDif.z += -Time.deltaTime;
            }

            if (Input.GetKey(GameController.GetInstance().InputManager.ForwardButton))
            {
                movementDif.x +=  Time.deltaTime;
            }

            if (Input.GetKey(GameController.GetInstance().InputManager.BackwardButton))
            {
                movementDif.x += -Time.deltaTime;
            }

            movementDif.Normalize();
            nextPos += movementDif * Speed;
            nextPos.x = Mathf.Clamp(nextPos.x, Boundary.XMin, Boundary.XMax);
            nextPos.z = Mathf.Clamp(nextPos.z, Boundary.YMin, Boundary.YMax);
            transform.position = nextPos;
        }
    }
}


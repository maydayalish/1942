﻿using UnityEngine;
using GGStudios.Controllers;

public class PoolObject : MonoBehaviour
{
    [SerializeField] protected MovementController movementController;

    public void Initialize()
    {
        movementController.OnGoBackPool += GoBackToPool;
    }

    protected void GoBackToPool()
    {
        StopAllCoroutines();
        CancelInvoke("InvokedStartRandomFire");
        CancelInvoke("InvokedStartOnTargetFire");
        GameController.GetInstance().ObjectPool.GoBackToPool(this);
    }
}

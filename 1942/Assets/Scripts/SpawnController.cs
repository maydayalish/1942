﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GGStudios.Controllers
{
    public class SpawnController : MonoBehaviour
    {
        [SerializeField] private Transform[] spawnPoints;
        [SerializeField] private Transform[] targetSpawnPoints;
        

        public void Initialize()
        {
            StartCoroutine(SpawnEnemyPlane());
            GameController.GetInstance().OnGameFinished += FinishGame;
        }
        
        public IEnumerator SpawnEnemyPlane()
        {
            yield return new WaitForSeconds(GameController.GetInstance().GetSpawnInterval());
            int levelPoint = GameController.GetInstance().GetLevelPlanePoint();
            int spawnPointIndex = UnityEngine.Random.Range(0, spawnPoints.Length);
            int fireType = UnityEngine.Random.Range(0, Enum.GetNames(typeof(FireType)).Length);
            GameController.GetInstance().ObjectPool.SpawnEnemyPlane(
                spawnPoints[spawnPointIndex].position, 
                targetSpawnPoints[spawnPointIndex].position, 
                levelPoint, 
                1, 
                (FireType)fireType
            );
            StartCoroutine(SpawnEnemyPlane());
        }

        public void FinishGame()
        {
            StopAllCoroutines();
        }

    }
}


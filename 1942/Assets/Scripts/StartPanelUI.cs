﻿using UnityEngine.UI;
using UnityEngine;
using GGStudios.Controllers;

public class StartPanelUI : MonoBehaviour
{
    public Text LoadingAssetName;
    [SerializeField] private Image progressBar;
    [SerializeField] private GameObject startButton;

    public void SetProgressBar(float progress)
    {
        progressBar.fillAmount = progress;
    }

    public void OnAssetsLoaded()
    {
        startButton.gameObject.SetActive(true);
        LoadingAssetName.text = "All assets loaded";
    }

    public void HandleStartButton()
    {
        gameObject.SetActive(false);
        GameController.GetInstance().Initialize();
    }
}

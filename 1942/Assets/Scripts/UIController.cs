﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    public Text ScoreText;
    public Text LevelText;
    public Image HealthFill;
    public Animator HealthAnimator;

    public GameOverPanelUI GameOverPanel;
    public StartPanelUI StartPanel;
    
    private int maxHealth;
    
    public void Initialize(int maxHealth)
    {
        this.maxHealth = maxHealth;
        HealthFill.fillAmount = 1;
        ScoreText.text = "SCORE: 0";
        LevelText.text = "LEVEL: 1";
        HealthAnimator.Play("Empty");
    }

    public void SetScore(int score)
    {
        ScoreText.text = "SCORE: " + score;
    }

    public void SetHealth(int health)
    {
        HealthFill.fillAmount = health / (float)maxHealth;
        if(health == 1)
        {
            HealthAnimator.Play("HealthWarning");
        }
    }

    public void SetLevel(int level)
    {
        LevelText.text = "LEVEL: " + level;
    }
	
}

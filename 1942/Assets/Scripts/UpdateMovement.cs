﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GGStudios.Controllers
{
    public class UpdateMovement : MovementController
    {
        public override void Move()
        {
            
        }

        // Update is called once per frame
        void Update()
        {
            Move();
            CheckOutOfBound();
        }
    }
}

